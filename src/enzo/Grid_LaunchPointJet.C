/***********************************************************************
/
/  GRID: LAUNCH JETS AROUND POINTJET PARTICLES
/  written by: S.Shiber and R. Iaconi
/  date:       Feb, 2018 based on Grid_AddFeedbackSphere
/
/  PURPOSE: Launch jets around all the PointJet Particles
/
************************************************************************/

#include <stdio.h>
#include <math.h>
#include <time.h>
#include "ErrorExceptions.h"
#include "macros_and_parameters.h"
#include "typedefs.h"
#include "global_data.h"
#include "Fluxes.h"
#include "GridList.h"
#include "ExternalBoundary.h"
#include "Grid.h"
#include "Hierarchy.h"
#include "CosmologyParameters.h"

int FindField(int field, int farray[], int numfields);

int grid::LaunchPointJet(pointjet PointJets)
{

  int dim, i, j, k, n, ip;
  int DensNum, GENum, TENum, Vel1Num, Vel2Num, Vel3Num;
  int n_cell;
  float m_cell, m_cell_new;
  float nx_L, ny_L, nz_L, radius, r_s, costheta;
  float xpos, ypos, zpos;
  float PointJetsVelocity, rho_jet;
  double pi = 4.0 * atan(1.0);
  int launchJet;

  if (MyProcessorNumber != ProcessorNumber)
    return SUCCESS;

  /* Find fields: density, total energy, velocity1-3. */

  if (this->IdentifyPhysicalQuantities(DensNum, GENum, Vel1Num, Vel2Num,
                                       Vel3Num, TENum) == FAIL) {
        ENZO_FAIL("Error in IdentifyPhysicalQuantities.");
  }

  for (ip=0; ip < PointJets.npoints; ip++) {

//    fprintf(stdout, "grid::LhaunchPointJet: Going Over PointJet %lld, Pos: (%g,%g,%g) ID: %lld\n", ip, PointJets.Position[ip][0],
//                                                                                                       PointJets.Position[ip][1], PointJets.Position[ip][2], PointJets.ID[ip]);
    n_cell = 0;
    m_cell = 0.0;
    m_cell_new = 0.0;
    launchJet = PointJets.JetActive[ip];

    if (launchJet)
    {
      radius = PointJets.JetLength[ip];

      /* set the directionn of the launching accroding to the pointjet particle */

      nx_L = PointJets.JetDir[ip][0];
      ny_L = PointJets.JetDir[ip][1];
      nz_L = PointJets.JetDir[ip][2];
      costheta = cos(PointJets.JetAngle[ip]);

      PointJetsVelocity = PointJets.JetVelocity[ip];
      
      for (k = 0; k < GridDimension[2]; k++) {
        zpos = CellLeftEdge[2][k] + 0.5*CellWidth[2][k]
          - PointJets.Position[ip][2];
        if (HydroMethod == Zeus_Hydro && GravitySolverType == GRAVITY_SOLVER_FAST)
          zpos -= 0.5*CellWidth[2][k];

        for (j = 0; j < GridDimension[1]; j++) {
          ypos = CellLeftEdge[1][j] + 0.5*CellWidth[1][j]
            - PointJets.Position[ip][1];
          if (HydroMethod == Zeus_Hydro && GravitySolverType == GRAVITY_SOLVER_FAST)
            ypos -= 0.5*CellWidth[1][j];

          for (i = 0; i < GridDimension[0]; i++) {
            xpos = CellLeftEdge[0][i] + 0.5*CellWidth[0][i]
              - PointJets.Position[ip][0];
            if (HydroMethod == Zeus_Hydro && GravitySolverType == GRAVITY_SOLVER_FAST)
              xpos -= 0.5*CellWidth[0][i];

            /* index */
            n = i + GridDimension[0]*(j + GridDimension[1]*k);

            /* Compute distance from center. */

            r_s = sqrt(xpos*xpos + ypos*ypos + zpos*zpos);
            if (r_s < radius) {
              if (fabs((xpos*nx_L + ypos*ny_L + zpos*nz_L)/r_s) > costheta) {

                 m_cell += BaryonField[DensNum][n] * pow(CellWidth[0][0], 3);

                /* Calculate the jet density */

                rho_jet = PointJets.JetMdot[ip] / (4 * pi * (1 - costheta) * pow(r_s, 2) * PointJetsVelocity);

                m_cell_new += rho_jet * pow(CellWidth[0][0], 3);
                n_cell++;

                /* Update velocities, density and TE; note that we now have kinetic (jet) energy added, so
                   for DualEnergyFormalism = 0 you don't have to update any energy field */

                BaryonField[Vel1Num][n] = PointJetsVelocity * xpos / r_s + PointJets.Velocity[ip][0];
                BaryonField[Vel2Num][n] = PointJetsVelocity * ypos / r_s + PointJets.Velocity[ip][1];
                BaryonField[Vel3Num][n] = PointJetsVelocity * zpos / r_s + PointJets.Velocity[ip][2];

                BaryonField[TENum][n] = BaryonField[TENum][n] * BaryonField[DensNum][n] / rho_jet;
                BaryonField[DensNum][n] = rho_jet;

                if (debug)
			fprintf(stdout, "grid::LhaunchPointJet: on processor %lld, jets density of %g injected in (%g, %g, %g) + 0.5*(%g, %g, %g) - (%g, %g, %g) = (%g, %g, %g)\n",
                                MyProcessorNumber, rho_jet, CellLeftEdge[0][i], CellLeftEdge[1][j], CellLeftEdge[2][k], CellWidth[0][i], CellWidth[1][j], CellWidth[2][k], PointJets.Position[ip][0], PointJets.Position[ip][1], PointJets.Position[ip][2], xpos, ypos, zpos);

                if (GENum >= 0 && DualEnergyFormalism)
                  for (dim = 0; dim < GridRank; dim++)
                    BaryonField[TENum][n] +=
                      0.5 * BaryonField[Vel1Num+dim][n] *
                      BaryonField[Vel1Num+dim][n];
              }
            }
          }
        }
      }// end: loop over grid
      
      if (n_cell > 0)
        fprintf(stdout, "grid::LhaunchPointJet: on processor %lld, jets injected (Mdot = %g Ms, JetsVelocity = %g, grid v = %g, along n_L = (%g, %g, %g)\nTotal Mass output = %g, Total Mass input = %g, Total diff = %g, cell modfied = %lld\n",
          MyProcessorNumber, PointJets.JetMdot[ip], PointJetsVelocity, BaryonField[Vel3Num][n_cell-1], nx_L, ny_L, nz_L, m_cell, m_cell_new, m_cell_new - m_cell, n_cell);
    }
  }  //

  return SUCCESS;

}
