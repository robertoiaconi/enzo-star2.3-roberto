#=======================================================================
#
# FILE:        Make.mach.raijin-intel
#
# DESCRIPTION: Makefile settings for NCI Raijin
#
# AUTHOR:      Roberto Iaconi (roberto.iaconi@students.mq.edu.au)
#
# DATE:        12 August 2013
#
#=======================================================================

MACH_TEXT  = Raijin with Intel compiler
MACH_VALID = 0
MACH_FILE  = Make.mach.raijin-intel

#-----------------------------------------------------------------------
# Paths
#-----------------------------------------------------------------------

HDF5_DIR = /apps/hdf5/1.8.10
MPI_DIR = /apps/openmpi/1.6.5
LOCAL_PYTHON_INSTALL = /short/w29/rxi552/yt-x86_64

#-----------------------------------------------------------------------
# Compiler settings
#-----------------------------------------------------------------------

MACH_CPP       = /usr/bin/cpp # C preprocessor command

# With MPI

MACH_CC_MPI    = $(MPI_DIR)/bin/mpicc  # C compiler when using MPI
MACH_CXX_MPI   = $(MPI_DIR)/bin/mpiCC  # C++ compiler when using MPI
MACH_FC_MPI    = $(MPI_DIR)/bin/mpif77 # Fortran 77 compiler when using MPI
MACH_F90_MPI   = $(MPI_DIR)/bin/mpif90 # Fortran 90 compiler when using MPI
MACH_LD_MPI    = $(MPI_DIR)/bin/mpiCC  # Linker when using MPI

# Without MPI

MACH_CC_NOMPI  = gcc   # C compiler when not using MPI
MACH_CXX_NOMPI = g++   # C++ compiler when not using MPI
MACH_FC_NOMPI  = g77   # Fortran 77 compiler when not using MPI
MACH_F90_NOMPI = ifort # Fortran 90 compiler when not using MPI
MACH_LD_NOMPI  = g++   # Linker when not using MPI

#-----------------------------------------------------------------------
# Machine-dependent defines
#-----------------------------------------------------------------------

# Note: When compiling against HDF5 version 1.8 or greater, you need to
# compile HDF5 with --with-default-api-version=v16, or Enzo with
# -DH5_USE_16_API.

MACH_DEFINES   = -DH5_USE_16_API -DLINUX # Defines for the architecture; e.g. -DSUN, -DLINUX, etc.

#-----------------------------------------------------------------------
# Compiler flag settings
#-----------------------------------------------------------------------

MACH_CPPFLAGS = -P -traditional # C preprocessor flags
MACH_CFLAGS   = # C compiler flags
MACH_CXXFLAGS = # C++ compiler flags
MACH_FFLAGS   = # Fortran 77 compiler flags
MACH_F90FLAGS = # Fortran 90 compiler flags
MACH_LDFLAGS  = # Linker flags

#-----------------------------------------------------------------------
# Precision-related flags
#-----------------------------------------------------------------------

MACH_FFLAGS_INTEGER_32 = -i4 #-fdefault-integer-4  Flag to force Fortran to use 32-bit integers
MACH_FFLAGS_INTEGER_64 = -i8 #-fdefault-integer-8 Flag to force Fortran to use 64-bit integers
MACH_FFLAGS_REAL_32    = -r4 #-fdefault-real-4 Flag to force Fortran to use 32-bit REALs
MACH_FFLAGS_REAL_64    = -r8 #-fdefault-real-8 Flag to force Fortran to use 64-bit REALs

#-----------------------------------------------------------------------
# Optimization flags
#-----------------------------------------------------------------------

MACH_OPT_WARN        = # Flags for verbose compiler warnings
MACH_OPT_DEBUG       = -g -O0 -fno-inline # Flags for debugging
MACH_OPT_HIGH        = # Flags for high conservative optimization
MACH_OPT_AGGRESSIVE  = # Flags for aggressive optimization

#-----------------------------------------------------------------------
# Includes
#-----------------------------------------------------------------------

LOCAL_INCLUDES_MPI    = -I $(MPI_DIR)/include # MPI includes
LOCAL_INCLUDES_HDF5   = -I $(HDF5_DIR)/include # HDF5 includes
LOCAL_INCLUDES_HYPRE  = # hypre includes
LOCAL_INCLUDES_PAPI   = # PAPI includes
LOCAL_INCLUDES_PYTHON = -I $(LOCAL_PYTHON_INSTALL)/include/python2.7/ \
                        -I $(LOCAL_PYTHON_INSTALL)/lib/python2.7/site-packages/numpy/core/include

MACH_INCLUDES         = $(LOCAL_INCLUDES_HDF5)

MACH_INCLUDES_MPI     = $(LOCAL_INCLUDES_MPI)
MACH_INCLUDES_HYPRE   = $(LOCAL_INCLUDES_HYPRE)
MACH_INCLUDES_PAPI    = $(LOCAL_INCLUDES_PAPI)
MACH_INCLUDES_PYTHON  = $(LOCAL_INCLUDES_PYTHON)

#-----------------------------------------------------------------------
# Libraries
#-----------------------------------------------------------------------

LOCAL_LIBS_MPI    = -L $(MPI_DIR)/lib -lmpi # MPI libraries
LOCAL_LIBS_HDF5   = -L $(HDF5_DIR)/lib -lhdf5  # HDF5 libraries
LOCAL_LIBS_HYPRE  = # hypre libraries
LOCAL_LIBS_PAPI   = # PAPI libraries
LOCAL_LIBS_PYTHON  = $(LOCAL_PYTHON_INSTALL)/lib/python2.7/config/libpython2.7.a #no -L here because the library file is given directly

LOCAL_LIBS_MACH   = -lirc -lifport -lifcore -limf -lsvml # Machine-dependent libraries

MACH_LIBS         = $(LOCAL_LIBS_HDF5) $(LOCAL_LIBS_MACH)
MACH_LIBS_MPI     = $(LOCAL_LIBS_MPI)
MACH_LIBS_HYPRE   = $(LOCAL_LIBS_HYPRE)
MACH_LIBS_PAPI    = $(LOCAL_LIBS_PAPI)
MACH_LIBS_PYTHON  = $(LOCAL_LIBS_PYTHON)