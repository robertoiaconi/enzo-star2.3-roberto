/***********************************************************************
/  GRID CLASS /
/  PURPOSE: initialize the grid for a binary common envelope run
/
/  RETURNS: FAIL or SUCCESS
/
************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <string.h>
#include "ErrorExceptions.h"
#include "macros_and_parameters.h"
#include "typedefs.h"
#include "phys_constants.h"
#include "global_data.h"
#include "Fluxes.h"
#include "GridList.h"
#include "ExternalBoundary.h"
#include "Grid.h"
#include "PointMass.h"

void WriteListOfInts(FILE *fptr, int N, int nums[]);
void WriteListOfFloats(FILE *fptr, int N, float floats[]);
int star_interpolation(float radius, float *density, float *pressure,
                       InitStarDataType InitStar);
int GetUnits(float *DensityUnits, float *LengthUnits,
	     float *TemperatureUnits, float *TimeUnits,
	     float *VelocityUnits, float *MassUnits, FLOAT Time);

int grid::CollapseTestJCInitializeGrid(int UseBaryons,
				       float InitialDensity, 
				       float InitialTemperature,
				       float SphereRadius,
				       float SphereDensity,
				       float SphereTemperature,
				       int CollapseTestJCSubgridsAreStatic)
{
  /* declarations */

  int dim, i, j, k, size, vel, field;
  float SphereCenter[3];
  float mu = 0.6;

  /* Create fields */

  if (UseBaryons) {  
    NumberOfBaryonFields = 0;
    FieldType[NumberOfBaryonFields++] = Density;
    FieldType[NumberOfBaryonFields++] = TotalEnergy;
    if (DualEnergyFormalism)
      FieldType[NumberOfBaryonFields++] = InternalEnergy;
    vel = NumberOfBaryonFields;
    FieldType[NumberOfBaryonFields++] = Velocity1;
    if (GridRank > 1)
      FieldType[NumberOfBaryonFields++] = Velocity2;
    if (GridRank > 2)
      FieldType[NumberOfBaryonFields++] = Velocity3;
    if (WritePotential)
      FieldType[NumberOfBaryonFields++] = GravPotential;
  }

  /* Return if this doesn't concern us. */
  
  if (ProcessorNumber != MyProcessorNumber)
    return SUCCESS;
  
  /* Get Units. */
  
  float TemperatureUnits = 1, DensityUnits = 1, LengthUnits = 1,
    VelocityUnits = 1, TimeUnits = 1, MassUnits = 1;
  
  if (GetUnits(&DensityUnits, &LengthUnits, &TemperatureUnits,
               &TimeUnits, &VelocityUnits, &MassUnits, Time) == FAIL)
    ENZO_FAIL("Error in GetUnits.\n");
  
  /* P=NkT/V mu_ion =  (1.4/2.1)* 1.6733e-24, kboltz = 1.380658e-16; */
  float PressureUnits = (VelocityUnits * VelocityUnits * DensityUnits) ;
  
  if (MyProcessorNumber == ROOT_PROCESSOR && debug)  {
    printf("DensityUnits      %g\n",DensityUnits);
    printf("MassUnits         %g\n",MassUnits);
    printf("LengthUnits       %g\n",LengthUnits);
    printf("TimeUnits         %g\n",TimeUnits);
    printf("VelocityUnits     %g\n",VelocityUnits);
    printf("TemperatureUnits  %g\n",TemperatureUnits);
    printf("PressureUnits     %g\n",PressureUnits);
  }
  
  /* Set the subgrid static flag. */
  
  SubgridsAreStatic = CollapseTestJCSubgridsAreStatic;
  

  /* Set gas */
  if (UseBaryons) {
   
    /* compute size of fields */
    size = 1;
    for (dim = 0; dim < GridRank; dim++)
      size *= GridDimension[dim];
    
    /* allocate fields */
    for (field = 0; field < NumberOfBaryonFields; field++)
      if (BaryonField[field] == NULL)
	BaryonField[field] = new float[size];

    /* Set velocities to zero */
    for (dim = 0; dim < GridRank; dim++)
      for (i = 0; i < size; i++)
	BaryonField[vel+dim][i] = 0.0;

    /* Set potential to zero */
    if (WritePotential)
      for (i = 0; i < size; i++)
	BaryonField[NumberOfBaryonFields-1][i] = 0.0;
    
    /* Set densities. */
    float density, temperature;
    float r =0.0, x=0.0, y = 0.0, z = 0.0;
      
    for (dim = 0; dim < GridRank; dim++) {
      SphereCenter[dim] = 0.5*(DomainLeftEdge[dim]+DomainRightEdge[dim]);
    }
    
    if (debug)
      printf("GridDimensions = [%"ISYM",%"ISYM",%"ISYM"]\n",
	     GridDimension[0]-6,GridDimension[1]-6,GridDimension[2]-6);

    float SphereMass = (4*pi/3)*pow((SphereRadius*LengthUnits), 3) *
      (SphereDensity*DensityUnits);

    printf("mass = %"GSYM", lunit = %"GSYM", dunit = %"GSYM", rho = %"GSYM", r = %"GSYM"\n",
	   SphereMass, LengthUnits, DensityUnits, SphereDensity,
	   SphereRadius);

    float VelocitySound = sqrt((SphereTemperature * Gamma * kboltz) /
			       (mu * mh)) / VelocityUnits;
    printf("\nVelocitySound (cm s^-1): %"GSYM"\n", VelocitySound * VelocityUnits);

    int index = 0;
        
    for (k = 0; k < GridDimension[2]; k++)
      for (j = 0; j < GridDimension[1]; j++)
	for (i = 0; i < GridDimension[0]; i++) {
	  
	  index = i + GridDimension[0]*(j + GridDimension[1]*k);
	    
	  /* Compute position */
          
	  x = CellLeftEdge[0][i] + 0.5*CellWidth[0][i];
	  if (GridRank > 1)
	    y = CellLeftEdge[1][j] + 0.5*CellWidth[1][j];
	  if (GridRank > 2)
	    z = CellLeftEdge[2][k] + 0.5*CellWidth[2][k];
          
	  /* Find distance from center. */
	  r = sqrt((x - SphereCenter[0]) * (x - SphereCenter[0]) +
		   (y - SphereCenter[1]) * (y - SphereCenter[1]) +
		   (z - SphereCenter[2]) * (z - SphereCenter[2]) );
          
	  density = InitialDensity;
          temperature = InitialTemperature;

	  // Inside the sphere
	  if (r < SphereRadius) {
	    
	    density = SphereDensity;
	    temperature = SphereTemperature;

	  } 

	  /* Density  BaryonField[0] */
	  BaryonField[0][index] = density;
          
	  /* Total SPECIFIC Energy BaryonField[1], don't forget the macroscopic kinetic energic */
	  // Velocities are 0
	  BaryonField[1][index] = temperature/TemperatureUnits/ ((Gamma-1.0)*mu);

	  /* Set internal energy if necessary. */
	  /* Since Velocities are set to 0, this is same as total energy */
	  if (DualEnergyFormalism)  
	    BaryonField[2][index] = BaryonField[1][index];

	}
    
  }  /* End of UseBaryons loop */

  return SUCCESS;
}
