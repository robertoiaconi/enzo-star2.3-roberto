/* This shouldn't be necessary but SGI static members don't work! */

#ifdef DEFINE_STORAGE
# define EXTERN
#else /* DEFINE_STORAGE */
# define EXTERN extern
#endif /* DEFINE_STORAGE */

EXTERN int GFAlreadyInitialized;      // Used for initializing
EXTERN int GFDimensions[MAX_NUMBER_OF_GREENS_FUNCTIONS][MAX_DIMENSION];
EXTERN gravity_boundary_type GFBoundary[MAX_NUMBER_OF_GREENS_FUNCTIONS];
EXTERN float *GFFunction[MAX_NUMBER_OF_GREENS_FUNCTIONS];
EXTERN int GFInUse[MAX_NUMBER_OF_GREENS_FUNCTIONS];
EXTERN int GFFunctionSize[MAX_NUMBER_OF_GREENS_FUNCTIONS];  // size in floats
EXTERN int GFIndex;                       // index for the next GF
EXTERN int GFTotalSize;                   // total number of floats used
