/***********************************************************************
/
/  GREENS FUNCTION CLASS (FIND OR CREATE THE APPROPRIATE GREENS FUNCTION)
/
/  written by: Greg Bryan
/  date:       March, 1995
/  modified1:
/
/  PURPOSE:
/
/  NOTE: 
/
************************************************************************/

#include <stdio.h>
#include <math.h>
#include "macros_and_parameters.h"
#include "typedefs.h"
#include "global_data.h"
#include "GreensFunction.h"

/* function prototypes */

int MakeGreensFunction(int Rank, int RealDims[], int Dims[], float *Function, 
		       gravity_boundary_type BoundaryType, 
		       int RefinementFactor);
//int MakeGreensFunctionTopGridIsolated(int Rank, int RealDims[], int Dims[], 
//				      float *Function);
void WriteListOfInts(FILE *fptr, int N, int nums[]);

#define DONT_USE_LOCK
#define UNLOCKED 0
#define LOCKED 1

static int GFLock = UNLOCKED;

int greens_function::PrepareGreensFunction(int Rank, int RealDims[], 
					   int Dims[],
					   gravity_boundary_type BoundaryType,
					   int RefinementFactor)
{

#ifdef USE_LOCK
  float a;
  printf("entering GF_Prep:\n");
  while (GFLock == LOCKED) {
    printf("+");
    for (int l = 0; l < 10000; l++)
      a += pow(-1, l)*sin(l);
  }

  GFLock = LOCKED;
#endif /* USE_LOCK */
  
  /* Error check. */

//  if (GreensFunctionThisGrid != NULL) {
//    fprintf(stderr, "GreensFunction->PrepareGreensFunction: Warning:\n");
//    fprintf(stderr, "  GreensFunctionThisGrid is not NULL.\n");
//  }

  /* Loop through list to see if we can find one that matches the desciption.
     This will save a lot of time, but storage of a lot of GFs take room. */

  int dim, i = -1, Match = FALSE;

  while (!Match && ++i < GreensFunctionMaxNumber) {
    if (GFBoundary[i] == BoundaryType) {
      Match = TRUE;
      for (dim = 0; dim < Rank; dim++)
	if (GFDimensions[i][dim] != Dims[dim])
	  Match = FALSE;
    }
  }

  /* If found, we don't have to do any work. */

  if (Match) {
    GFInUse[i]++;
    GreensFunctionThisGrid = GFFunction[i];
    GreensFunctionIndex    = i;
    GFLock = UNLOCKED;
    return SUCCESS;
  }

  /* Error check. */

  if (GreensFunctionMaxNumber < 1) {
    fprintf(stderr, "GreensFunctionMaxNumber must be > 1!\n");
    return FAIL;
  }

  /* If not, we'll compute a new one and put it in the spot specified by
     Index (wrapping the counter if necessary). */

  if (GFIndex > GreensFunctionMaxNumber-1)
    GFIndex = min(1, GreensFunctionMaxNumber-1);  // set to 1 if possible
  int nchecks = 0;
  while (GFInUse[GFIndex] > 0 && nchecks++ < GreensFunctionMaxNumber)
    GFIndex = (GFIndex+1) % GreensFunctionMaxNumber;
  if (nchecks == GreensFunctionMaxNumber) {
    fprintf(stderr, "GF: no empty GreensFunction.\n");
    return FAIL;
  }

  /* Add one to index and set boundary to undefined so nobody uses or over-
     runs this spot. */

  int ThisGF         = GFIndex++;
  GFBoundary[ThisGF] = GravityUndefined;
  GFInUse[ThisGF]    = 1;

  /* If the spot is already being used, delete the old stuff. */

  if (GFFunction[ThisGF] != NULL) {
    delete GFFunction[ThisGF];
    GFTotalSize -= GFFunctionSize[ThisGF];   // reduce size by this amount
    if (debug) {
      printf("GreensFunction: deleting GF[%d] with dims ", ThisGF);
      WriteListOfInts(stdout, Rank, GFDimensions[ThisGF]);
    }
  }

  /* Fill in the new spot. */

  int size = RealDims[0]/2 + 1;
  for (dim = 0; dim < Rank; dim++) {
    GFDimensions[ThisGF][dim] = Dims[dim];
    if (dim > 0)
      size *= RealDims[dim];
  }
  GFFunctionSize[ThisGF] = size;
  GFTotalSize           += size;
  GFFunction[ThisGF]     = new float[size];  // allocate space for G.F.
  if (debug) {
    printf("GreensFunction: creating GF[%d] with dims ", ThisGF, size);
    WriteListOfInts(stdout, Rank, Dims);
  }
  if (GFFunction[ThisGF] == NULL) {
    fprintf(stderr, "malloc error (out of memory?)\n");
    return FAIL;
  }

  /* Create the field */

  if (BoundaryType == TopGridIsolated) {
    fprintf(stderr, "isolated gravity is not supported.\n");
    return FAIL;
    //    if (MakeGreensFunctionTopGridIsolated(Rank, RealDims, Dims, 
    //			     GFFunction[ThisGF]) == FAIL) {
    //      fprintf(stderr, "Error in MakeGreensFunctionTopGridIsolated.\n");
    //      return FAIL;
    //    }
  }
  else
    if (MakeGreensFunction(Rank, RealDims, Dims, GFFunction[ThisGF], 
			   BoundaryType, RefinementFactor) == FAIL) {
      fprintf(stderr, "Error in MakeGreensFunction.\n");
      return FAIL;
    }

  /* Set the pointer to the new function. */

  GreensFunctionThisGrid = GFFunction[ThisGF];
  GreensFunctionIndex    = ThisGF;

  /* Set the boundary type only when done. */

  GFBoundary[ThisGF]     = BoundaryType;

  if (debug)
    printf("GreensFunction: finished creating GF[%d].\n", ThisGF);

#ifdef USE_LOCK
  GFLock = UNLOCKED;
#endif /* USE_LOCK */
  return SUCCESS;
}
