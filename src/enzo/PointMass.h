/***********************************************************************
/
/  POINTMASS STRUCTURE
/
/  written by: j oishi (incorporated by JC Passy 21/01/10)
/  date:       October, 2007
/  modified1:
/
/  PURPOSE: list of all the point masses position and the total number of particles
/           Each particle  is tied to a specific id number and mass
/
/  REQUIRES: macros_and_parameters.h
/
************************************************************************/
#ifndef POINTMASS_H
#define POINTMASS_H

#define MAX_POINTMASS_PARTICLES 512

struct pointmass
{
  int ID[MAX_POINTMASS_PARTICLES];
  float Position[MAX_POINTMASS_PARTICLES][MAX_DIMENSION];
  float Velocity[MAX_POINTMASS_PARTICLES][MAX_DIMENSION];
  int Level[MAX_POINTMASS_PARTICLES];
  float Mass[MAX_POINTMASS_PARTICLES];
  int npoints;
};

#endif

