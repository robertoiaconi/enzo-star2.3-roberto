/***********************************************************************
/
/  Binary Star initial data
/
/  written by: j. oishi
/  date:       Sept, 2007
/  modified1:  JC Passy April, 2010
/
/  PURPOSE:
/    a container to hold the 1-D data to construct a stellar model
/
************************************************************************/
#ifndef BINARYSTARDATA_H
#define BINARYSTARDATA_H

#define MAX_STAR_DATA_SHELLS 4096

struct InitStarDataType {
  float Radius[MAX_STAR_DATA_SHELLS]; 
  float Density[MAX_STAR_DATA_SHELLS];
  float Temp[MAX_STAR_DATA_SHELLS];
  float Pressure[MAX_STAR_DATA_SHELLS];
  float Mass[MAX_STAR_DATA_SHELLS];
  float totalmass;
  float radius;
  int nshells;
};
#endif
