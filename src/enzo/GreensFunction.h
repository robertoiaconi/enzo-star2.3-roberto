/***********************************************************************
/
/  GREENS FUNCTION CLASS
/
/  written by: Greg Bryan
/  date:       March, 1995
/  modified1:
/
/  PURPOSE:
/
************************************************************************/

#include "GreensFunction_global.h"

class greens_function
{
 private:

  float *GreensFunctionThisGrid;          // pointer for this grid.
  int    GreensFunctionIndex;

//  static int AlreadyInitialized;      // Used for initializing
//  static int Dimensions[MAX_NUMBER_OF_GREENS_FUNCTIONS][MAX_DIMENSION];
//  static gravity_boundary_type Boundary[MAX_NUMBER_OF_GREENS_FUNCTIONS];
//  static float *Function[MAX_NUMBER_OF_GREENS_FUNCTIONS];
//  static int FunctionSize[MAX_NUMBER_OF_GREENS_FUNCTIONS];  // size in floats
//  static int Index;                       // index for the next GF
//  static int TotalSize;                   // total number of floats used

 public:

  /* Constructor (clear static stuff if this is the first time called). */

  greens_function();

  /* Prepare Greens Function */

  int PrepareGreensFunction(int Rank, int RealDims[], int Dims[],
			    gravity_boundary_type BoundaryType,
			    int RefinementFactor);

  /* Multiplies the given (complex k-space) field with the appropriate G.F. */

  int MultiplyGreensFunction(int Rank, int RealDims[], int Dims[], 
			     float *field);

  /* Compute the acceleration field over the k-space potential by multipling
     with D(k).  The result goes in AccelerationField. */

  int ComputeAcceleration(int Rank, int RealDims[], int Dims[], int Direction,
			  float *Potential, float *AccelerationField,
			  float CellWidth);

  void Release() {GFInUse[GreensFunctionIndex]--;};
};






