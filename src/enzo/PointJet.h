/***********************************************************************
/
/  POINTJET STRUCTURE
/
/  written by: s shiber
/  date:       January, 2018
/  modified1:
/
/  PURPOSE: list of all the jet masses position and the total number of particles
/           Each particle  is tied to a specific id number and mass
/
/  REQUIRES: macros_and_parameters.h
/
************************************************************************/
#ifndef POINTJET_H
#define POINTJET_H

#define MAX_POINTJET_PARTICLES 512

struct pointjet
{
  int ID[MAX_POINTJET_PARTICLES];
  float Position[MAX_POINTJET_PARTICLES][MAX_DIMENSION];
  float Velocity[MAX_POINTJET_PARTICLES][MAX_DIMENSION];
  int Level[MAX_POINTJET_PARTICLES];
  float Mass[MAX_POINTJET_PARTICLES];
  int JetActive[MAX_POINTJET_PARTICLES];
  float JetDir[MAX_POINTJET_PARTICLES][MAX_DIMENSION];
  float JetVelocity[MAX_POINTJET_PARTICLES];
  float JetAngle[MAX_POINTJET_PARTICLES];
  float JetLength[MAX_POINTJET_PARTICLES];
  float JetMdot[MAX_POINTJET_PARTICLES];
  int npoints;

};

#endif

